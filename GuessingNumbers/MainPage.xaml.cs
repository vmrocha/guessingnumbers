﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using GuessingNumbers.Resources;
using GuessingNumbers.ViewModels;
using Microsoft.Phone.Controls;

namespace GuessingNumbers
{
    public partial class MainPage : PhoneApplicationPage
    {
        private IList<List<int>> steps;
        private int currentStep;
        private int accumulator;

        public MainPage()
        {
            this.InitializeComponent();

            this.steps = new List<List<int>>
            {
                new List<int> { 1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 43, 45, 47, 49, 51, 53, 55, 57, 59},
                new List<int> { 2, 3, 6, 7, 10, 11, 14, 15, 18, 19, 22, 23, 26, 27, 30, 31, 34, 35, 38, 39, 42, 43, 46, 47, 50, 51, 54, 55, 58, 59 },
                new List<int> { 4, 5, 6, 7, 12, 13, 14, 15, 20, 21, 22, 23, 28, 29, 30, 31, 36, 37, 38, 38, 39, 44, 45, 46, 47, 52, 53, 54, 55, 60 },
                new List<int> { 8, 9, 10, 11, 12, 13, 14, 15, 24, 25, 26, 27, 28, 29, 30, 31, 40, 41, 42, 43, 44, 45, 46, 47, 56, 57, 58, 59, 60 },
                new List<int> { 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60 },
                new List<int> { 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60 }
            };

            this.AddNumbers();
        }

        private void AddNumbers(int step = 0)
        {
            int row = 0;

            gridNumbers.Children.Clear();

            gridNumbers.ColumnDefinitions.Clear();
            gridNumbers.ColumnDefinitions.Add(new ColumnDefinition());
            gridNumbers.ColumnDefinitions.Add(new ColumnDefinition());
            gridNumbers.ColumnDefinitions.Add(new ColumnDefinition());
            gridNumbers.ColumnDefinitions.Add(new ColumnDefinition());
            gridNumbers.ColumnDefinitions.Add(new ColumnDefinition());

            gridNumbers.RowDefinitions.Clear();

            for (int i = 0; i < this.steps[step].Count; row++)
            {
                gridNumbers.RowDefinitions.Add(new RowDefinition());

                for (int col = 0; col < 5 && i < this.steps[step].Count; col++)
                {
                    Border border = new Border();
                    border.Child = new TextBlock
                    {
                        Text = this.steps[step][i].ToString(),
                        HorizontalAlignment = HorizontalAlignment.Center,
                        VerticalAlignment = VerticalAlignment.Center
                    };

                    Grid.SetRow(border, row);
                    Grid.SetColumn(border, col);

                    gridNumbers.Children.Add(border);

                    i++;
                }
            }
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            if (this.panorama.SelectedIndex == 1)
            {
                this.panorama.SelectedIndex = 0;
                e.Cancel = true;
            }

            base.OnBackKeyPress(e);
        }

        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            this.panorama.SelectedIndex = 1;
        }

        private void ButtonYes_Click(object sender, RoutedEventArgs e)
        {
            this.accumulator += this.steps[this.currentStep].First();
            this.NextStep();
        }

        private void ButtonNo_Click(object sender, RoutedEventArgs e)
        {
            this.NextStep();
        }

        private void ButtunRestart_Click(object sender, RoutedEventArgs e)
        {
            this.panorama.SelectedIndex = 0;
            this.panorama.SelectedIndex = 1;
        }

        private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.MoveToStep(0, false);
            this.accumulator = 0;
            ViewModelLocator.MainViewModel.DisplayResult = false;
        }

        private void NextStep()
        {
            if (this.currentStep == this.steps.Count - 1)
            {
                if (this.accumulator > 60)
                {
                    MessageBox.Show(AppResources.InvalidNumber);
                    this.MoveToStep(0);
                    return;
                }

                ViewModelLocator.MainViewModel.Result = this.accumulator;
                ViewModelLocator.MainViewModel.DisplayResult = true;

                this.AnimationTurn(this.gridResult, TurnstileTransitionMode.BackwardIn);
            }
            else
            {
                this.MoveToStep(this.currentStep + 1);
            }
        }

        private void MoveToStep(int step, bool animate = true)
        {
            if ((this.currentStep != step) && (step < this.steps.Count))
            {
                this.AddNumbers(step);

                this.txtSteps.Text = string.Format("{0} / {1}", step + 1, this.steps.Count);

                this.currentStep = step;

                if (animate)
                {
                    this.AnimationSlide(this.gridNumbers);
                }
            }
        }

        private void AnimationSlide(UIElement element)
        {
            SlideTransition slideTransition = new SlideTransition { Mode = SlideTransitionMode.SlideLeftFadeIn };
            ITransition transition = slideTransition.GetTransition(element);
            transition.Completed += delegate { transition.Stop(); };
            transition.Begin();
        }

        private void AnimationTurn(UIElement element, TurnstileTransitionMode mode, Action action = null)
        {
            TurnstileTransition turnstile = new TurnstileTransition { Mode = mode };
            ITransition transition = turnstile.GetTransition(element);
            transition.Completed += delegate
            { 
                transition.Stop(); 
                if (action != null)
                {
                    action();
                }
            };
            transition.Begin();
        }
    }
}