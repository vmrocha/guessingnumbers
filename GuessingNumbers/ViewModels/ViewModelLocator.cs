﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingNumbers.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            MainViewModel = new MainViewModel();
        }

        public static MainViewModel MainViewModel
        {
            get;
            private set;
        }
    }
}
