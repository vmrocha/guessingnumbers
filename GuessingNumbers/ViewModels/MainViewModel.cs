﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingNumbers.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private bool displayResult;
        private int result;

        public MainViewModel()
        {
            if (DesignerProperties.IsInDesignTool)
            {
                this.DisplayResult = true;
            }
        }

        public bool DisplayResult
        {
            get { return this.displayResult; }
            set { this.SetProperty(ref this.displayResult, value); }
        }

        public int Result
        {
            get { return this.result; }
            set { this.SetProperty(ref this.result, value); }
        }
    }
}
