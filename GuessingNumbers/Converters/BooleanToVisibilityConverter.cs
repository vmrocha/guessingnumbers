﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace GuessingNumbers.Converters
{
    public sealed class BoolToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
            {
                return null;
            }

            bool invertResult = false;
            Boolean.TryParse(parameter as string, out invertResult);

            bool boolValue = invertResult ? !(bool)value : (bool)value;

            return boolValue ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool invertResult = false;
            Boolean.TryParse(parameter as string, out invertResult);

            if (Equals(value, Visibility.Visible))
            {
                return invertResult ? false : true;
            }

            if (Equals(value, Visibility.Collapsed))
            {
                return invertResult ? true : false;
            }

            return null;
        }
    }
}
